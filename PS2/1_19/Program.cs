﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_19
{
    class Program
    {
        
        static double SummaPI(double eps, out int iterations, out double time)
        {
            double sum = 0;
            double item = 1.0 / 2;
            int m = 1;
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            while (Math.Abs(item) > eps)
            {
                sum += item;
                m++;
                item *= ((m - 1.0) * (m - 1.0)) / ((2.0 * m) * (2.0 * m - 1.0));
            }
            sw.Stop();
            iterations = m;
            time = sw.Elapsed.TotalMilliseconds;
            return sum;
        }
        static void Main()
        {
            
            double eps = 0.0000000000000001;            
            int iterations;
            double time;
            double result = SummaPI(eps, out iterations, out time);
                   
            Console.WriteLine(result);
            Console.WriteLine(iterations);            
            Console.WriteLine(time);
        }
        
    }
}
