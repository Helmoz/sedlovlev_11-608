﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Math;

namespace _4_9
{
    class Program
    {
        static double F(double x)
        {
            return  Sqrt(Tan(x));
        }

        static double IntegrateLRect(double a, double b, int n, out double time)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            if (b < a || n <= 0)
                throw new ArgumentException("Invalid arguments");

            double delta = (b - a) / n;
            double sum = 0;
            for (int i = 0; i < n; i++)
                sum += F(a + i * delta);
            sw.Stop();
            time = sw.Elapsed.TotalMilliseconds;
            return delta * sum;
        }
        static double IntegrateRRect(double a, double b, int n, out double time)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            if (b < a || n <= 0)
                throw new ArgumentException("Invalid arguments");

            double delta = (b - a) / n;
            double sum = 0;
            for (int i = 1; i <= n; i++)
                sum += F(a + i * delta);
            sw.Stop();
            time = sw.Elapsed.TotalMilliseconds;
            return delta * sum;
        }
        static double IntegrateTrap(double a, double b, int n, out double time)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            if (b < a || n <= 0)
                throw new ArgumentException("Invalid arguments");

            double delta = (b - a) / (n);
            double sum = 0;
            double prev = F(a);
            double cur = 0;
            for (int i = 1; i <= n; i++)
            {
                cur = F(a + i * delta);
                sum += (cur + prev);
                prev = cur;
            }
            sw.Stop();
            time = sw.Elapsed.TotalMilliseconds;
            return delta * sum / 2;
        }

        static double IntegrateSimp(double a, double b, int n, out double time)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            if (b < a || n <= 0 || n % 2 == 1)
                throw new ArgumentException("Invalid arguments");

            double delta = (b - a) / n;
            n /= 2;

            double sum1 = 0;
            for (int i = 1; i < n; i++)
            {
                sum1 += F(a + 2 * i * delta);
            }

            double sum2 = 0;
            for (int i = 1; i <= n; i++)
            {
                sum2 += F(a + 2 * (i - 1) * delta);
            }
            sw.Stop();
            time = sw.Elapsed.TotalMilliseconds;
            return delta / 3 * ((F(a) + F(b) + 2 * sum1 + 4 * sum2));
        }
        static double IntegrateMCarlo(double a, double b, int n, out double time)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            if (b < a || n <= 0)
                throw new ArgumentException("Invalid arguments");

            double delta = (b - a) / n;
            double sum = 0;
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
                sum += F(a + rnd.NextDouble() * (b - a));
            sw.Stop();
            time = sw.Elapsed.TotalMilliseconds;
            return sum * delta;
        }
        static void Main()
        {
            double result;
            double time;
            result = IntegrateLRect(0, 1.5, 10000000, out time);
            Console.WriteLine("Left Rectangles" + ": " + result + "\tTime:"+time);

            result = IntegrateRRect(0, 1.5, 10000000, out time);
            Console.WriteLine("Right Rectangles" + ": " + result + "\tTime:" + time);

            result = IntegrateTrap(0, 1.5, 10000000, out time);
            Console.WriteLine("Trapeze method" + ": " + result + "\tTime:" + time);

            result = IntegrateSimp(0, 1.5, 10000000, out time);
            Console.WriteLine("Simpsom method" + ": " + result + "\t\tTime:" + time);

            result = IntegrateMCarlo(0, 1.5, 10000000, out time);
            Console.WriteLine("MCarlo method " + ": " + result + "\tTime:" + time);
        }
    }
}
