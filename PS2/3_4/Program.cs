﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_3
{
    class Program
    {
        static double Log(double x, double eps, out int iterations, out double time)
        {
            double a = x;
            double b = 1;
            double currentY=0;
            double prevY=0;   
            int k = 0;
            
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            while (a < 2)
            {
                k++;
                a *= a;
            }
            currentY = b;
            while (currentY-prevY > eps)
            {
                currentY = prevY+b;
            }
            sw.Stop();
            iterations = k - 1;
            time = sw.Elapsed.TotalMilliseconds;
            return currentY;
        }
        static void Main()
        {
            double x = double.Parse(Console.ReadLine());
            double eps = 0.000000000001;
            int iterations;
            double time;
            double result = Log(x,eps, out iterations, out time);

            Console.WriteLine(result);
            Console.WriteLine(iterations);
            Console.WriteLine(time);
        }
    }
}
