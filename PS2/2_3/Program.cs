﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_3
{
    class Program
    {
        static double Pi(double eps, out int iterations, out double time)
        {
            double sum = 0;
            double item = 1;
            int k = 0;
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            while (Math.Abs(item) > eps)
            {
                sum += item;
                k++;
                item *= (-1*(2*k-1))/((2*k+1)*3.0);
            }
            sw.Stop();
            iterations = k - 1;
            time = sw.Elapsed.TotalMilliseconds;
            return 2*Math.Sqrt(3)*sum;
        }
        static void Main()
        {
            double eps = 0.000000000000001;
            int iterations;
            double time;
            double result = Pi(eps, out iterations, out time);
            Console.WriteLine(result);
            Console.WriteLine(Math.PI);
            Console.WriteLine(iterations);
            Console.WriteLine(time);
        }   
    }
}
