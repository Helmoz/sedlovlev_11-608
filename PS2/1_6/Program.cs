﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_6
{
    class Program
    {
        static double SummaArctg(double x, double eps, out int iterations, out double time)
        {
            double sum = 0;
            double item = x;   
            int k = 0;
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            while (Math.Abs(item) > eps)
            {
                sum += item;
                k++;
                item *= (-1*x*x*(2*k-1))/(2*k+1);

                ;        
            }
            sw.Stop();
            iterations = k - 1;
            time = sw.Elapsed.TotalMilliseconds;
            return sum;
        }
        static void Main()
        {
            double x = double.Parse(Console.ReadLine());
            double eps = 0.00000001;
            int iterations;
            double time;
            double result = SummaArctg(x, eps, out iterations, out time);

            Console.WriteLine(result);
            Console.WriteLine(iterations);
            Console.WriteLine(time);
        }
    }
}
