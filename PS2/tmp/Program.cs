﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tmp
{
    class Program
    {
        static double B(int k)
        {
            if (k > 0)
            {
                return B((k - 1) / 2);
            }
            return 1;
        }
        static void Main(string[] args)
        {
            Console.WriteLine(B(5));
        }
    }
}
