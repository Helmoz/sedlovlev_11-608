﻿using System;
using System.IO;


public class Rational
{
    public int Numerator { get; private set; }
    public int Denominator { get; private set; }
    public int Sign { get; private set; }

    public Rational(int n = 0, int d = 1)
    {
        Numerator = n;
        Denominator = d;
        if ((n > 0 && d > 0) || (n < 0 && d < 0))
            Sign = 1;
        else
            Sign = -1;
    }

    public override string ToString()
    {
        if (Sign < 0)
            return "-" + Math.Abs(Numerator) + "/" + Math.Abs(Denominator);
        else
            return Numerator + "/" + Denominator;
    }

    private static int GCD(int x, int y)
    {
        while (y > 0)
            y = x % (x = y);
        return x;
    }

    private static int LCM(int x, int y)
    {
        return x * y / GCD(x, y);
    }

    private static bool Compare(Rational first, Rational second)
    {
        bool check = true;
        var save = new Rational(first.Numerator, first.Denominator);
        var tmp1 = new Rational(first.Numerator, first.Denominator);
        var tmp2 = new Rational(second.Numerator, second.Denominator);
        tmp1.Numerator *= tmp2.Denominator;
        tmp1.Denominator *= tmp2.Denominator;
        tmp2.Numerator *= save.Denominator;
        tmp2.Denominator *= save.Denominator;
        if (tmp1.Numerator < tmp2.Numerator)
            check = false;
        return check;

    }

    private static Rational Addition(Rational x, Rational y)
    {
        if (x.Sign == y.Sign)
        {
            int mult = LCM(Math.Abs(x.Denominator), Math.Abs(y.Denominator));
            var tmp = new Rational(Math.Abs(x.Numerator) * mult / Math.Abs(x.Denominator) + Math.Abs(y.Numerator) * mult / Math.Abs(y.Denominator), mult);
            mult = GCD(tmp.Numerator, tmp.Denominator);
            tmp.Numerator /= mult;
            tmp.Denominator /= mult;
            if (x.Sign > 0)
                return tmp;
            else
            {
                tmp.Sign = -1;
                return tmp;
            }
        }
        else
        {
            int mult = LCM(x.Denominator, y.Denominator);
            var tmp = new Rational(x.Numerator * mult / x.Denominator + y.Numerator * mult / y.Denominator, mult);
            mult = GCD(tmp.Numerator, tmp.Denominator);
            tmp.Numerator /= mult;
            tmp.Denominator /= mult;
            return tmp;
        }


    }

    private static Rational Substraction(Rational x, Rational y)
    {
        if (x.Sign < 0 && y.Sign > 0)
        {
            x.Sign = 1;
            var Ntmp = new Rational();
            Ntmp = x + y;
            Ntmp.Sign = -1;
            return Ntmp;
        }
        if (x.Sign > 0 && y.Sign < 0)
        {
            var AbsY = new Rational(Math.Abs(y.Numerator), Math.Abs(y.Denominator));
            return x + AbsY;
        }
        if (x.Sign < 0 && y.Sign < 0)
        {
            var AbsY = new Rational(Math.Abs(y.Numerator), Math.Abs(y.Denominator));
            bool tmp = Compare(x, y);
            var Curtmp = x + AbsY;
            if (!tmp)
                Curtmp.Sign = -1;
            return Curtmp;
        }
        else
        {
            int mult = LCM(x.Denominator, y.Denominator);
            var tmp = new Rational(x.Numerator * mult / x.Denominator - y.Numerator * mult / y.Denominator, mult);
            mult = GCD(tmp.Numerator, tmp.Denominator);
            tmp.Numerator /= mult;
            tmp.Denominator /= mult;
            return tmp;
        }

    }

    private static Rational Multiplication(Rational x, Rational y)
    {
        int mult = LCM(x.Denominator, y.Denominator);
        var tmp = new Rational(Math.Abs(x.Numerator) * Math.Abs(y.Numerator), Math.Abs(x.Denominator) * Math.Abs(y.Denominator));
        mult = GCD(tmp.Numerator, tmp.Denominator);
        tmp.Numerator /= mult;
        tmp.Denominator /= mult;
        if (x.Sign == y.Sign)
            return tmp;
        else
        {
            tmp.Sign = -1;
            return tmp;
        }
    }

    private static Rational Division(Rational x, Rational y)
    {
        int mult = LCM(x.Denominator, y.Denominator);
        var tmp = new Rational(Math.Abs(x.Numerator) * Math.Abs(y.Denominator), Math.Abs(x.Denominator) * Math.Abs(y.Numerator));
        mult = GCD(tmp.Numerator, tmp.Denominator);
        tmp.Numerator /= mult;
        tmp.Denominator /= mult;
        if (x.Sign == y.Sign)
            return tmp;
        else
        {
            tmp.Sign = -1;
            return tmp;
        }
    }

    public static Rational operator +(Rational x, Rational y)
    {
        return Addition(x, y);
    }

    public static Rational operator -(Rational x, Rational y)
    {
        return Substraction(x, y);
    }

    public static Rational operator *(Rational x, Rational y)
    {
        return Multiplication(x, y);
    }

    public static Rational operator /(Rational x, Rational y)
    {
        return Division(x, y);
    }
}

public class Calculator
{
    public enum Operation { Input = 1, Save, Load, Action, Check, Pop, Clear, Exit }

    public enum Action { Addition = 1, Substraction, Multiplication, Division }

    public enum State { Operation, Number, SaveCell, LoadCell }

    private State _state = State.Operation;

    private Rational[] _stack = new Rational[8];

    private int _stackTop = 0;

    public void PushBack(Rational x)
    {
        _stack[_stackTop++] = x;
        _state = State.Operation;
    }

    public Rational Pop()
    {
        var tmp = _stack[_stackTop - 1];
        _stack[_stackTop - 1] = null;
        _stackTop--;
        return tmp;
    }

    private Rational[] _memory = new Rational[8];

    private void PrintOperations()
    {
        Console.WriteLine("Type operation number");
        for (int i = 1; i < 9; i++)
            Console.WriteLine(i + ": " + (Operation)i);
    }

    private void PrintActions()
    {
        Console.WriteLine("Type action number");
        for (int i = 1; i < 5; i++)
            Console.WriteLine(i + ": " + (Action)i);
    }

    private bool Parse(string str, out Rational r)
    {
        string[] tmp = str.Split(' ', '/');
        try
        {
            r = new Rational(int.Parse(tmp[0]), int.Parse(tmp[1]));
            return true;
        }
        catch (Exception)
        {
            r = new Rational();
            return false;
        }
    }



    public void Start()
    {
        PrintOperations();
        bool exit = false;
        string str;
        while (!exit)
        {
            str = Console.ReadLine();
            switch (_state)
            {
                case State.Operation:

                    Operation o = (Operation)int.Parse(str);

                    switch (o)
                    {
                        case Operation.Input:
                            Console.WriteLine("Type rational number");
                            _state = State.Number;
                            break;
                        case Operation.Save:
                            _state = State.SaveCell;
                            break;
                        case Operation.Load:
                            _state = State.LoadCell;
                            break;
                        case Operation.Action:
                            PrintActions();
                            string ActTmp = Console.ReadLine();
                            Action act = (Action)int.Parse(ActTmp);
                            switch (act)
                            {
                                case Action.Addition:
                                    Rational SumTmp1, SumTmp2, SumRes;
                                    SumTmp1 = Pop();
                                    SumTmp2 = Pop();
                                    SumRes = SumTmp1 + SumTmp2;
                                    PushBack(SumRes);
                                    _state = State.Operation;
                                    break;
                                case Action.Substraction:
                                    Rational SubsTmp1, SubsTmp2, SubsRes;
                                    SubsTmp1 = Pop();
                                    SubsTmp2 = Pop();
                                    SubsRes = SubsTmp1 - SubsTmp2;
                                    PushBack(SubsRes);
                                    _state = State.Operation;
                                    break;
                                case Action.Multiplication:
                                    Rational MultiTmp1, MultiTmp2, MultiRes;
                                    MultiTmp1 = Pop();
                                    MultiTmp2 = Pop();
                                    MultiRes = MultiTmp1 * MultiTmp2;
                                    PushBack(MultiRes);
                                    _state = State.Operation;
                                    break;
                                case Action.Division:
                                    Rational DivTmp1, DivTmp2, DivRes;
                                    DivTmp1 = Pop();
                                    DivTmp2 = Pop();
                                    DivRes = DivTmp1 / DivTmp2;
                                    PushBack(DivRes);
                                    _state = State.Operation;
                                    break;
                            }
                            break;
                        case Operation.Pop:
                            if (_stackTop < 1) break;
                            _stack[_stackTop - 1] = null;
                            _stackTop--;
                            break;
                        case Operation.Check:
                            Console.Write("Stack: | ");
                            for (int i = 0; i < _stack.Length; i++)
                                Console.Write(_stack[i] + " | ");
                            Console.WriteLine();
                            Console.WriteLine();
                            Console.Write("Memory: | ");
                            for (int i = 0; i < _memory.Length; i++)
                                Console.Write(_memory[i] + " | ");
                            Console.WriteLine();
                            break;
                        case Operation.Clear:
                            Console.Clear();
                            PrintOperations();
                            break;
                        case Operation.Exit:
                            exit = true;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();

                    }
                    break;

                case State.Number:
                    Rational r;
                    bool b = Parse(str, out r);
                    if (!b)
                        Console.WriteLine("Input error! " + _state + " expected");
                    PushBack(r);
                    _state = State.Operation;
                    break;

                case State.SaveCell:
                    Console.WriteLine("Choose cell of memory(a-h)");
                    string tmp = Console.ReadLine();
                    switch (tmp)
                    {
                        case "a":
                            _memory[0] = Pop();
                            _state = State.Operation;
                            break;
                        case "b":
                            _memory[1] = Pop();
                            _state = State.Operation;
                            break;
                        case "c":
                            _memory[2] = Pop();
                            _state = State.Operation;
                            break;
                        case "d":
                            _memory[3] = Pop();
                            _state = State.Operation;
                            break;
                        case "e":
                            _memory[4] = Pop();
                            _state = State.Operation;
                            break;
                        case "f":
                            _memory[5] = Pop();
                            _state = State.Operation;
                            break;
                        case "g":
                            _memory[6] = Pop();
                            _state = State.Operation;
                            break;
                        case "h":
                            _memory[7] = Pop();
                            _state = State.Operation;
                            break;
                        default:
                            Console.WriteLine("Wrong cell");
                            _state = State.SaveCell;
                            break;
                    }
                    break;

                case State.LoadCell:
                    Console.WriteLine("Choose cell of memory(a-h)");
                    string Stmp = Console.ReadLine();
                    switch (Stmp)
                    {
                        case "a":
                            PushBack(_memory[0]);
                            _memory[0] = null;
                            _state = State.Operation;
                            break;
                        case "b":
                            PushBack(_memory[1]);
                            _memory[1] = null;
                            _state = State.Operation;
                            break;
                        case "c":
                            PushBack(_memory[2]);
                            _memory[2] = null;
                            _state = State.Operation;
                            break;
                        case "d":
                            PushBack(_memory[3]);
                            _memory[3] = null;
                            _state = State.Operation;
                            break;
                        case "e":
                            PushBack(_memory[4]);
                            _memory[4] = null;
                            _state = State.Operation;
                            break;
                        case "f":
                            PushBack(_memory[5]);
                            _memory[5] = null;
                            _state = State.Operation;
                            break;
                        case "g":
                            PushBack(_memory[6]);
                            _memory[6] = null;
                            _state = State.Operation;
                            break;
                        case "h":
                            PushBack(_memory[7]);
                            _memory[7] = null;
                            _state = State.Operation;
                            break;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();

            }
        }
    }
}

class Program
{
    static void Main()
    {
        var calc = new Calculator();
        calc.Start();
    }
}
