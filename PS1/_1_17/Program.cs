﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_17
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            int left = 0, right = 0;
            for (int i = 0; i < 6; i++)
            {
                if (i % 2 == 0)
                    right += number % 10;
                else
                    left += number % 10;
                number /= 10;
            }
            if (left == right + 1 || left == right - 1)
                Console.WriteLine("YES");
            else
                Console.WriteLine("NO");
        }
    }
}
