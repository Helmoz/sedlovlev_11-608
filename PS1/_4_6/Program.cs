﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_6
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            
            for (int a = 1; a < n; a++)
            {
                int sumA = 0, sumB = 0;
                for (int i = 1; i * i <= a; i++)
                {
                    if (a % i == 0)
                        sumA += i + a / i;
                }
                for (int b = a; b<n;b++)
                {
                    if (a + b < n)
                    {
                        for (int i = 1; i * i <= b; i++)
                        {
                            if (b % i == 0)
                                sumB += i + b / i;
                        }
                        if (sumA == sumB && sumA == a + b)
                            Console.WriteLine(a + " " + b);
                        sumB = 0;
                    }
                    

                }

            }
        }
    }
}
