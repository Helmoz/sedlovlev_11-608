﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_12
{
    class Program
    {
        static void Main(string[] args)
        {
            int tmp = int.Parse(Console.ReadLine());
            int min = tmp;
            int Mcounter = 0;
            while (tmp != 0)
            {
                if (tmp <= min)
                {
                    if (tmp != min)
                        Mcounter = 0;
                    min = tmp;
                    Mcounter++;
                }
                tmp = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Minimal number: " + min+ "\r\n"+ "Amount: "+Mcounter);
        }
    }
}
