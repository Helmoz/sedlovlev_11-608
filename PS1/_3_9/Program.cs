﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_9
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int save = int.Parse(Console.ReadLine());
            int control=1;
            for (int i = 0; i < n-1; i++)
            {
                int tmp = int.Parse(Console.ReadLine());
                if (tmp * save >= 0)
                    control = 0;
                save = tmp;
            }
            if (control == 1)
                Console.WriteLine("YES");
            else
                Console.WriteLine("NO");
        }
    }
}
