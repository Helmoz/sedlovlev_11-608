﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_19
{
    class Program
    {
        static void Main(string[] args)          
        {
            
            long ans=1;
            long number = long.Parse(Console.ReadLine());
            for (long i = number; i >= 0; i--)
            {
                if (number % i == 0)
                {
                    bool prime = true;
                    for (long j = 2; j*j <= i; j++)
                        if (i % j == 0)
                        {
                            prime = false;
                            break;
                        }
                    if (prime)
                    {
                        ans = i;
                        break;
                    }
                        
                }
            }
            Console.WriteLine(ans);

        }
    }
}
