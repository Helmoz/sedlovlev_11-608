﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_15
{
    class Program
    {
        static void Main(string[] args)
        {
            int k = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            int count0 = 0, count1 = 0, count2=0, count3=0, count4=0;
            while (n > 0)
            {
                switch(n%k)
                {
                    case 0:
                        count0++;
                        break;
                    case 1:
                        count1++;
                        break;
                    case 2:
                        count2++;
                        break;
                    case 3:
                        count3++;
                        break;
                    case 4:
                        count4++;
                        break;
                }
                n /= k;
            }
            int max = count0;
            int output = 0;
            if (count1 > max)
            {
                max = count1;
                output = 1;
            }                
            if (count2 > max)
            {
                max = count2;
                output = 2;
            }
            if (count3 > max)
            {
                max = count3;
                output = 3;
            }
            if (count4 > max)
            {
                output = 4;
            }
            Console.WriteLine(output);
        }
    }
}
