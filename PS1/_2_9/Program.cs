﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_9
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            int counter = 0;
            while (number > 0)
            {
                if (number % 2 == 1)
                    counter++;
                number /= 2;
            }
            Console.WriteLine(counter);
        }
    }
}