﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_13
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int counter = 0;
            while(n>0)
            {
                int i = (int)Math.Sqrt(n);                
                n -= i * i;
                counter++;
            }
            Console.WriteLine(counter);
            
        }
    }
}
