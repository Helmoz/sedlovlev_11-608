﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_8
{
    class Program
    {
        static void Main()
        {
            double x1 = Convert.ToDouble((Console.ReadLine()));
            double y1 = Convert.ToDouble((Console.ReadLine()));
            double r1 = Convert.ToDouble((Console.ReadLine()));
            double x2 = Convert.ToDouble((Console.ReadLine()));
            double y2 = Convert.ToDouble((Console.ReadLine()));
            double r2 = Convert.ToDouble((Console.ReadLine()));
            double dist = ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            if ((r1 + r2) * (r1 + r2) >= dist)
                Console.WriteLine("YES");
            else
                Console.WriteLine("NO");
        }
    }
}