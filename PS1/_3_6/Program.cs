﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_6
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int save = int.Parse(Console.ReadLine()); ;
            int control = 0;
            for(int i=0; i < n-1; i++)
            {
                int tmp = int.Parse(Console.ReadLine());
                if (tmp <= save)
                    control++;
                save = tmp;
            }
            if (control > 0)
                Console.WriteLine("NO");
            else
                Console.WriteLine("YES");
        }
    }
}
